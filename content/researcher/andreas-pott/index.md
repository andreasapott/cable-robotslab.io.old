---
title: "Andreas Pott"
date: 2020-06-20T16:46:50+02:00
toc: true
organization:
    - ustutt-isw
location: Düsseldorf, Germany
social:
   - link: https://www.researchgate.net/profile/Andreas_Pott
     icon: fe-book
   - link: https://scholar.google.com/citations?user=LG4DFPsAAAAJ&hl=de&oi=sra
     icon: fe-external-link
   - link: https://gitlab.cc-asp.fraunhofer.de/wek/wirex
     icon: fe-gitlab
   - link: https://de.linkedin.com/in/andreas-pott-96b49b175
     icon: fe-linkedin
resources:
   - name: cover
     src: cover.jpg
   - name: avatar
     src: avatar.jpg
---
Andreas Pott is lecturer at the University of Stuttgart (Germany) in the field of robotics. He is working on cable-driven parallel robots since more than 15 years. In 2009, he was the leading developer for the IPAnema 1 robot. Since then, a whole family of cable robots evolved from this design.

Within the field of cable robotics, his main research topics are
- statics and force distributions
- kinematic codes
- workspace
- robot design (topology, parameter, hardware)
- application of cable robots
- software for cable robots

Many methods and algorithms published in papers and the book have been implemented in the software library `WireX` with its main package `wirelib` and the GUI `WireCenter`. The python interface to wirelib called WiPy is a cross platform project running under Windows and Linux. WireCenter is a graphical editors and visualization tools solely for the Windows platform. Since 2019, WireX is available as open source under MIT license. It is hosted on gitlab as [WireX](https://gitlab.cc-asp.fraunhofer.de/wek/wirex). 

Together with Tobias Bruckmann, Andreas is chairman of the *International Conference on Cable-driven Parallel Robots* (CableCon) started in 2012. Since then the conferences was held mostly biannual in Duisburg (2014, Germany), Laval (2016, Canada), and Krakow (2019, Poland). The fifth CableCon will be hosted by LIRMM, Montpellier, France in 2021. Andreas authored some 100 articles on cable robots as well as the overview book [Cable-driven Parallel Robots: Theory and Application](https://www.springer.com/de/book/9783319761374).

Highlight in the development cable robots demonstrators developed over the years are

- IPAnema 1 (2009). First demonstrator based on a custom yet industrial winch design with an industry grade real-time controller [YouTube](https://www.youtube.com/watch?v=RCa8uDFzbsw)
- IPAnema 2 (2010): The video shows the applications of the cable-driven parallel robot IPAnema 2 for handling and assembly of collectors in solar-thermal power plants. Cable robots can be scaled towards very large workspaces and payloads thus allowing automating assembly processes where not other robotic systems are yet available. 
The IPAnema 2 systems was presented to the public at the trade fair Automatica 2010 in Munich by Fraunhofer IPA. [YouTube](https://www.youtube.com/watch?v=9-qQYv90_Og)

- IPAnema 3 Mini (2014): Smale scale cable robot with eight cable and advanced force control. [YouTube](https://www.youtube.com/watch?v=9YeBULHbCJM)

- IPAnema 3 robot: IPAnema 3 is the first robot in the IPAnema family with the industrial quality IPAnema 3 winch system and control system based on Beckhoff Twincat 3. Using these components, a number of different applications has been implemented. The IPAnema 3 winches allow for cable robots with a maximum design size of some 50 meters (for 2.5 mm cables) and some 20 meters (for 6 mm cables). The nominal maximum cable force is rate to be 3000 N allowing for payloads of up to 300 kg in typical geometric designs.

- Expo 2015: Two cable robots at the German Pavilion for the show *Be(e)active*. Contributed the robot planning and design, programming and path validation tooling, as well as control and safety concept [YouTube](https://www.youtube.com/watch?v=cCf7mELTKrc). More than one million visitors saw the show.

- IPAnema 3 robot (continued): A late offspring of the family is the large scale 3D printer for concrete that as lately demonstrated by the [HINCON](http://www.hindcon3d.com/) project. Results from 3D printing with cable robots can be seen on [YouTube](https://www.youtube.com/watch?v=PZPbd5WC6lQ).
