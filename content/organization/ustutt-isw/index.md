---
title: Institute for Control Engineering of Machine Tools and Manufacturing Units
date: 2020-04-27T17:49:57+02:00
short_title: ISW
institution: University of Stuttgart
short_institution: UStutt
location: Stuttgart, Germany
website: https://www.isw.uni-stuttgart.de
resources: 
    - name: cover
      src: cover.jpg
    - name: avatar
      src: avatar.png
---

**We Control the Future**

The Institute for Control Engineering of Machine Tools and Manufacturing Units (ISW) of the University of Stuttgart is one of the leading research centers in the field of control engineering – from planning to the tool.
The ISW does interdisciplinary research in technologies for the future production and automation.
For the industry we are for almost 50 years an innovative and reliably partner for ambitious challenges, from the first idea to the end product.
