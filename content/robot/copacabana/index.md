---
title: "COPacabana"
date: 2020-04-27T18:16:58+02:00
long_title: Cable OPerated Robot
description: "Two separate robots, one for handling and the other one for inspection."
motionpattern: 3r3t
organization: ustutt-isw
superseded: copacabana-c16
resources:
    - src: 'gallery/IMG_0739.jpg'
      title: 'COPacabana platforms #1'
      params:
        description: "Medium close-up of the two platforms of COPacabana: the **inspection platform** in the front and the **handling platform** in the back."
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_0759.jpg'
      title: 'COPacabana long shot'
      params:
        description: "Long shot of COPacabana: the actuating winches in the front, the two platforms on their home base plates in the middle, and the two control cabinets in the background."
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_0771.jpg'
      title: 'COPacabana platforms #2'
      params:
        description: "Medium close-up of the two platforms with the rear/inspection platform elevated at about half the workspace height."
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_0777.jpg'
      title: 'COPacabana inspection platform'
      params:
        description: "Medium close shot of the rear/inspection plaform elevated at about half the workspace height."
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_0807.jpg'
      title: 'COPacabana platform-side cable anchor'
      params:
        description: "Close-up of COPacabana's platform-sided cable anchor realized as a swivel bolt-like system much like the one found on IPAnema."
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_0816.jpg'
      title: 'COPacabana winch'
      params:
        description: "Close-up of the travelling carriage on the IPAnema/COPacabana winches showing the cable approaching the inner/first pulley which is mounted on a force sensor."
        copyright: ISW, University of Stuttgart
    - src: 'gallery/IMG_0819.jpg'
      title: 'COPacabana platforms'
      params:
        description: "Medium long shot of both platforms of COPacabana in mid-workspace position. Both platforms can be operated independently of each other using separate control systems."
        copyright: ISW, University of Stuttgart
---
Comprising two separate robots, *COPacabana Inspection (COP-IF)* and *COPacabana Handling (COP-HN)*, COPacabana is a test setup for various scenarios in manufacturing and production environments.
Designed to be used cooperatively, COP-HN can load and unload a CNC milling machine with a piece taken from or placed onto the platform of COP-IF.
Both systems are based on the IPAnema robot family and use the same hardware design for pulleys, winches, and platform.

Various student theses have been conducted on the system including manipulation of the end effector pose using a smartphone or automated positioning and orienting using a macro camera.