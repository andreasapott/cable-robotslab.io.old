---
title: Motion Patterns
---

{{< latex >}}
The motion pattern of a robot characterizes a subset of virtual displacements $\delta \boldsymbol{y}$--a superposition of translation and rotation i.e., rigid body motion in three-dimensional space--the end-effector can execute in consistency with its kinematic constraints.
The Euclidean motion group $SE(3)$ consists of six independent such virtual displacements thus maximum number of degrees-of-freedom $n$ of a mobile platform is $n = 6$.
Generally, the motion pattern is represented by an abbreviation of the form $n_{r} \text{R} \, n_{t} \text{T}$, where $\text{R}$ represents $n_{r}$ rotational degrees-of-freedom and $\text{T}$ stands for $n_{t}$ translational degrees-of-freedom, respectively.
If the robot cannot perform rotational motion, $n_{r}$ may be omitted for clarity.
{{< /latex >}}
