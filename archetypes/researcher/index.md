---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
toc: true
organization:
    - tudelft-pme
    - ustutt-isw
location: 'Where?'
website: https://philipptempel.me
social:
   - link: https://github.com/philipptempel
     icon: fe-github
   - link: https://instagram.com/phlpptmpl
     icon: fe-instagram
---

**Insert Lead paragraph here.**



## Documentation

### `organization`

Assign the researcher to an organization by simply putting its slug (i.e., directory name or file name under `/content/organization/`) in this field.
A researcher can be assigned more than one organization, and these will be rendered in the order they are defined in `organization`.

#### Sample
```yaml
organization:
    - tudelft-pme
    - lirmm
```


### `location`

Where the researcher lives.
Is a markdown enabled field.

#### Sample
```yaml
location: Delft, the Netherlands
```


### `website`

Address of either a personal or professional website of the researcher.

#### Sample
```yaml
website: 'https://philipptempel.me'
```


### `social`

If the researcher has any social links they would like to show, you can add them here.
The field is a simple list of mappings, each mapping containing the `icon` and `link` field.
Field `link` defines the link to the remote page and `icon` a valid icon from the [https://feathericons.com/](Feather Icons list).

#### Sample
```yaml
social:
    - link: https://gitlab.com/cable-robots
      icon: fe-gitlab
    - link: https://google.com
      icon: fe-external-link
```


### `resources`

Want to add resources like files, images, avatar, or a cover image to the profile?
These go into this section.

#### Cover Image

By convention, each page can have a cover image that shows whatever you want it to show.
Make sure your image content fits into a 1200x200 image as these are the fixed dimensions of the cover image.
Two field names must be present for the resource to be identified as a cover image.
These are `name` with value `cover` and `src` with the image source relative to the profile's directory.

##### Sample
```yaml
resources:
    - name: cover
      src: "cover.jpg"
```

#### Avatar

An avatar is a small depiction of the researcher which is used in various places on the website.
Pick something that makes this researcher stand out from the other ones and that is easily identifiable.
All avatars are rendered with square side dimensions (in fact, the base dimensions are 600x600), so make sure your avatar fits this shape.
Two field names must be present for the resource to be identified as an avatar.
These are `name` with value `avatar` and `src` with the image source relative to the researcher's directory.

##### Sample

```yaml
resources:
    - name: avatar
      src: "avatar.jpg"
```
